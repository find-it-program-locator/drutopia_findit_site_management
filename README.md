<!-- writeme -->
Drutopia Findit Site Management
===============================

A collection of capabilities used by site managers, including copying content, converting content from one type to another, and other such wizardry not for mere content editors.

 * https://gitlab.com/find-it-program-locator/drutopia_findit_site_management
 * Issues: https://gitlab.com/find-it-program-locator/drutopia_findit_site_management/issues
 * Source code: https://gitlab.com/find-it-program-locator/drutopia_findit_site_management/tree/8.x-1.x
 * Keywords: find it, drupal, drutopia, site management
 * Package name: drupal/drutopia_findit_site_management


### Requirements

 * drupal/config_actions ^1.0-beta1
 * drupal/drutopia_core ^1.0-alpha1
 * drupal/rabbit_hole ^1.0@beta
 * drupal/simple_entity_merge ^1.0
 * drupal/quick_node_clone ^1.12
 * drupal/convert_bundles ^1.0@alpha
 * drupal/coffee ^1.0@beta


### License

GPL-2.0+

<!-- endwriteme -->
