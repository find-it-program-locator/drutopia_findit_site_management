<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function drutopia_findit_site_management_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    // Provide links to other report-like features, primarily at admin/content.
    case 'drutopia_findit_site_management.admin_reports':
      $markup .= '<div class="messages">';
      $markup .= '<p class="messages__header">' . t('See also:') . '</p>';
      $markup .= '<div class="messages__content">';
      $markup .= '<ul>';
      // Hide convoluted code for checking and printing a URL based on route.
      $markup .= drutopia_findit_site_management_output_li_link_if_route('view.findit_locations_admin_list.page');
      $markup .= drutopia_findit_site_management_output_li_link_if_route('view.findit_organizations_admin_list.page');
      $markup .= '</ul>';
      $markup .= '</div>';
      $markup .= '</div>';
      return $markup;

  }
}

/**
 * Implements hook_form_FORM_ID_alter() for system_site_information_settings.
 *
 * Remove the field for setting the site slogan, because our theme, Newtowne,
 * does not use it.  Instead, the Find It Program Locator distribution opts to
 * provide a better site manager experience by putting everything they can
 * manage regarding the display of the front page within a content
 *
 * This form alter can't go in the theme that ignores the slogan because we
 * need this form alter to apply to the admin theme!
 *
 * @TODO move this to its own contrib module when themes can add dependencies on modules.
 */
function drutopia_findit_site_management_form_system_site_information_settings_alter(&$form, FormStateInterface $form_state) {
  $form['site_information']['site_slogan']['#access'] = FALSE;
}

/**
 * Restore the user's page title to the user account.
 *
 * The contacts module changes the user's page title with the First and
 * last name this is not something desired in this project so this commit
 * changes it back to the original value.
 */
function drutopia_findit_site_management_user_format_name_alter(&$name, AccountInterface $account) {
  if ($account->isAnonymous()) {
    return;
  }
  $name = $account->getAccountName();
}

/**
 * Implements hook_module_implements_alter().
 */
function drutopia_findit_site_management_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter' && isset($implementations['drutopia_findit_site_management'])) {
    // Move this module's implementation of form_alter to the end of the list.
    // We do this so our implementation of hook_form_BASE_FORM_ID_alter()
    // is called AFTER field_group's [should check just which code]
    // which contains the code we are trying to alter.
    $hook_implementation = $implementations['drutopia_findit_site_management'];
    unset($implementations['drutopia_findit_site_management']);
    $implementations['drutopia_findit_site_management'] = $hook_implementation;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node_form.
 *
 * Completely hide the Meta vertical tab (field group) from service providers.
 *
 * @TODO Move Encontrarlo theme's manipulation of meta field group here.
 */
function drutopia_findit_site_management_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // If the current user has the permission, do not hide the Meta vertical tab.
  if (\Drupal::currentUser()->hasPermission('access meta tab')) {
    return;
  }

  $bundle = $form_state->getFormObject()->getEntity()->bundle();
  if (!in_array($bundle, ['findit_organization', 'findit_program', 'findit_event', 'findit_place'])) {
    return;
  }

  foreach ($form['#fieldgroups']['group_findit_meta']->children as $form_element_id) {
    $form[$form_element_id]['#access'] = FALSE;
  }

  // The advanced items are hard-coded so we move them into the meta field group
  // in our encontrarlo theme (see encontrarlo_form_alter) which runs after this
  // so we need to put the access denied here.
  $form['advanced']['#access'] = FALSE;

}

/**
 * Helper function to print a list item linking to given route, if available.
 */
function drutopia_findit_site_management_output_li_link_if_route($route) {
  /* @var \Drupal\Core\Routing\RouteProviderInterface $route_provider */
  $route_provider = \Drupal::service('router.route_provider');
  if ($route_provider->routeExists($route)) {
    $route_object = $route_provider->getRouteByName($route);
    $title = $route_object->getDefault('_title');
    return '<li> ' . \Drupal\Core\Link::createFromRoute($title, $route)->toString() . '</li>';
  }
}
